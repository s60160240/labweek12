const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  company: String,
  title: String,
  category: String,
  number: String,
  date: String,
  address: String,
  time: String
})

module.exports = mongoose.model('User', userSchema)
